/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 17:33:40 by jwong             #+#    #+#             */
/*   Updated: 2018/11/12 11:19:20 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "mlx.h"
#include "libft.h"
#include "wolf3d.h"

void	load_texture(t_win *win)
{
    const char *textures[] = {TEX_BRICK, TEX_CELTIC, TEX_COLOURS, TEX_STONE, TEX_GND, TEX_SKY};
    int i;

	win->tex = (t_texture *)malloc(sizeof(t_texture) * NUM_TEX);
	if (win->tex == NULL)
		error_handler(MALLOC_ERR);
	i = 0;
	while (i < NUM_TEX)
	{
		ft_bzero(&win->tex[i], sizeof(t_texture));
		if (!(win->tex[i].xpm = mlx_xpm_file_to_image(win->mlx,\
						(char *)textures[i], &win->tex[i].width,\
						&win->tex[i].height)))
			error_handler(TEXTURE);
		if (!(win->tex[i].img = mlx_get_data_addr(win->tex[i].xpm,\
						&win->tex[i].bpp, &win->tex[i].size_line,\
						&win->tex[i].endian)))
			error_handler(TEXTURE);
		i++;
	}
}

void	load_colours(t_win *win)
{
  const int	colours[] = {WALL_NORTH, WALL_EAST, WALL_SOUTH, WALL_WEST};
  int			i;

  win->colours = (int *)malloc(sizeof(int) * NUM_WALL);
  if (win->colours == NULL)
    error_handler(MALLOC_ERR);
  i = 0;
  while (i < NUM_WALL)
  {
    win->colours[i] = colours[i];
    i++;
  }
}
