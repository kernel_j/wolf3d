/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 18:31:11 by jwong             #+#    #+#             */
/*   Updated: 2018/11/09 12:47:19 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "wolf3d.h"

int		calculate_len(int len)
{
	int new_len;

	if ((len % 2) > 0)
		new_len = (len / 2) + 1;
	else
		new_len = len / 2;
	return (new_len);
}

int		get_wall(t_player *player)
{
	int wall;

	wall = 0;
	if (player->hit == 1)
	{
		if (player->h_ray_dir_y > 0)
			wall = SOUTH;
		else
			wall = NORTH;
	}
	else if (player->hit == 2)
	{
		if (player->v_ray_dir_x > 0)
			wall = WEST;
		else
			wall = EAST;
	}
	return (wall);
}

int		find_start(double height)
{
	int	start;

	start = (MAX_WIN / 2) - (height / 2);
	return (start);
}

double	calculate_distance(double dx, double dy)
{
	double dist;

	dist = sqrt(pow(dx, 2) + pow(dy, 2));
	return (dist);
}

double	calculate_height(double dist)
{
	double height;

	if (dist > 0)
		height = WALL_LEN / dist * PLANE_DIST;
	else
		height = MAX_WIN;
	return (height);
}
