/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/07 11:26:58 by jwong             #+#    #+#             */
/*   Updated: 2018/11/08 17:41:34 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "libft.h"
#include "wolf3d.h"

void	error_system(void)
{
	char	*err;

	err = "Error: ";
	write(1, err, ft_strlen(err));
	err = strerror(errno);
	write(1, err, ft_strlen(err));
	write(1, "\n", 1);
	exit(-1);
}

void	error_program(char *msg)
{
	write(1, msg, ft_strlen(msg));
}

int		error_handler(int err)
{
	if (err == ARG_ERR)
		error_program(ERRMSG_ARG);
	else if (err == FD_ERR || err == MALLOC_ERR)
		error_system();
	else if (err == FILE_ERR)
		error_program(ERRMSG_FILE);
	else if (err == FILE_EMPTY)
		error_program(ERRMSG_EMPTY_FILE);
	else if (err == TEXTURE)
	{
		error_program(ERRMSG_TEXTURE);
		exit(-1);
	}
	return (-1);
}
