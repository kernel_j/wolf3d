/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda_vertical.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 12:43:43 by jwong             #+#    #+#             */
/*   Updated: 2018/11/09 12:44:25 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "libft.h"
#include "wolf3d.h"

double	get_triangle_angle_vertical(double angle)
{
	double new_angle;

	if (angle >= 0 && angle <= 90.0)
		new_angle = 0 - angle;
	else if (angle > 90.0 && angle <= 180.0)
		new_angle = 180.0 - angle;
	else if (angle > 180.0 && angle <= 270.0)
		new_angle = 180.0 - angle;
	else
		new_angle = 360.0 - angle;
	return (new_angle);
}

double	vertical_intersect_up(t_map *map, t_player *player, t_dda *dda)
{
	double	dist;
	int		hit;

	dda->new_y = player->pos_y + dda->dy;
	player->v_ray_dir_x = dda->dx;
	player->v_ray_dir_y = dda->dy;
	while (!(hit = is_hit(map, dda->new_x, floor(dda->new_y))))
	{
		dda->dx += 1;
		dda->new_x = player->pos_x + dda->dx;
		dda->dy = dda->dx * tan(dda->triangle_rad);
		dda->new_y = player->pos_y + dda->dy;
	}
	if (hit == 2)
		dist = -1;
	else
	{
		dist = calculate_distance(dda->dx, dda->dy);
		player->v_hitx = dda->new_x;
		player->v_hity = dda->new_y;
	}
	return (dist);
}

double	vertical_intersect_down(t_map *map, t_player *player, t_dda *dda)
{
	double	dist;
	int		hit;

	dda->new_y = player->pos_y + dda->dy;
	player->v_ray_dir_x = dda->dx;
	player->v_ray_dir_y = dda->dy;
	while (!(hit = is_hit(map, dda->new_x - 1, floor(dda->new_y))))
	{
		dda->dx -= 1;
		dda->new_x = player->pos_x + dda->dx;
		dda->dy = dda->dx * tan(dda->triangle_rad);
		dda->new_y = player->pos_y + dda->dy;
	}
	if (hit == 2)
		dist = -1;
	else
	{
		dist = calculate_distance(dda->dx, dda->dy);
		player->v_hitx = dda->new_x;
		player->v_hity = dda->new_y;
	}
	return (dist);
}

void	vertical_init_dda(t_dda *dda, t_player *player, double angle, bool up)
{
	double	triangle_angle;

	if (up == true)
		dda->new_x = ceil(player->pos_x);
	else
		dda->new_x = floor(player->pos_x);
	triangle_angle = get_triangle_angle_vertical(angle);
	dda->triangle_rad = triangle_angle * M_PI / 180;
	dda->dx = dda->new_x - player->pos_x;
	dda->dy = dda->dx * tan(dda->triangle_rad);
}

double	vertical_intersect(t_map *map, t_player *player, double angle)
{
	t_dda	dda;
	double	dist;

	ft_bzero(&dda, sizeof(t_dda));
	if (angle <= 90.0 || angle >= 270.0)
	{
		vertical_init_dda(&dda, player, angle, true);
		dist = vertical_intersect_up(map, player, &dda);
	}
	else if (angle > 90.0 && angle < 270.0)
	{
		vertical_init_dda(&dda, player, angle, false);
		dist = vertical_intersect_down(map, player, &dda);
	}
	else
		dist = 0;
	return (dist);
}
