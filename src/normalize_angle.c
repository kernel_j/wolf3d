double  normalize_angle(double angle)
{
    if (angle < 0)
    {
        while (angle < 0)
            angle += 360.0;
    }
    else
    {
        while (angle > 360)
            angle -= 360;
    }
    return (angle);
}
