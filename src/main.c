/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 15:22:17 by jwong             #+#    #+#             */
/*   Updated: 2018/11/08 17:27:50 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include "mlx.h"
#include "libft.h"
#include "wolf3d.h"

int     print_debug(t_win *win)
{
    char str[1000];

    sprintf(str, "pos x: %.2lf y: %.2lf angle: %.2lf", win->game->player->pos_x,
            win->game->player->pos_y, win->game->player->angle);
    if (win->control->render_mode)
        mlx_string_put(win->mlx, win->win, 500, 10, 0, str);
    else
        mlx_string_put(win->mlx, win->win, 500, 10, 16777215, str);
    return (1);
}

int	redraw(t_win *win)
{
	mlx_destroy_image(win->mlx, win->img_id);
	win->img_id = mlx_new_image(win->mlx, MAX_WIN, MAX_WIN);
	win->img = mlx_get_data_addr(win->img_id, &win->bpp,
			&win->size_line, &win->endian);
	raycast(win);
	mlx_put_image_to_window(win->mlx, win->win, win->img_id, 0, 0);
    print_debug(win);
	move_player(win->control, win->game->player, win->game->map);
	return (0);
}

int	game_loop(t_win *win)
{
	mlx_hook(win->win, 2, (1L << 0), key_press, win);
	mlx_hook(win->win, 3, (1L << 1), key_release, win);
	mlx_hook(win->win, 6, (1L << 6), mouse_over_handler, win);
	mlx_hook(win->win, 17, (1L << 17), exit_handler, win);
	mlx_loop_hook(win->mlx, redraw, win);
	mlx_loop(win->mlx);
	return (0);
}

int	main(int argc, char **argv)
{
	t_win		win;
	t_game		game;
	t_map		map;
	t_player	player;
	t_control	control;

	if (argc != 2)
		return (error_handler(ARG_ERR));
	init_game(&win, &game, &map, &player);
	if (setup(argv, &win, &control) == FALSE)
	{
		printf("error\n");
		return (1);
	}
	load_texture(&win);
	load_colours(&win);
	raycast(&win);
	game_loop(&win);
	return (0);
}
