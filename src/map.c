/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/07 11:48:08 by jwong             #+#    #+#             */
/*   Updated: 2018/11/08 17:31:18 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#include "libft.h"
#include "wolf3d.h"

#include <stdio.h>

int		**init_map(t_win *win)
{
	int	**map;

	map = (int **)malloc(sizeof(int *) * win->game->map->row);
	if (!map)
		error_handler(MALLOC_ERR);
	return (map);
}

int		total_entries(char *str)
{
	int	len;

	len = ft_strlen(str);
	return (calculate_len(len));
}

int		*extract_map(t_win *win, char *str)
{
	int	*row;
	int	len;
	int	i;
	int	j;

	len = total_entries(str);
	row = (int *)malloc(sizeof(int) * len);
	if (!row)
		error_handler(MALLOC_ERR);
	if (win->game->map->col == 0)
		win->game->map->col = len;
	i = -1;
	j = 0;
	while (str[++i])
	{
		if (str[i] != ' ')
		{
			if (str[i] == 'P')
				row[j] = 0;
			else
				row[j] = atoi(&str[i]);
			j++;
		}
	}
	return (row);
}

void	load_map(char *file, t_win *win)
{
	int		fd;
	int		i;
	char	*buff;

	if ((fd = open(file, O_RDONLY)) == -1)
	{
		close(fd);
		error_handler(FD_ERR);
	}
	buff = NULL;
	win->game->map->map = init_map(win);
	i = 0;
	while (get_next_line(fd, &buff) > 0)
	{
		win->game->map->map[i] = extract_map(win, buff);
		i++;
		free(buff);
		buff = NULL;
	}
}
