/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 18:40:14 by jwong             #+#    #+#             */
/*   Updated: 2018/11/09 15:11:57 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "mlx.h"
#include "libft.h"
#include "wolf3d.h"

void	put_pixel(t_win *win, int x, int y, int rgb)
{
	int size;

	if (x >= MAX_WIN || x < 0 || y >= MAX_WIN || y < 0)
		return ;
	size = (y * win->size_line) + (x * win->bpp / 8);
	if (win->endian == 0)
	{
		win->img[size] = mlx_get_color_value(win->mlx, rgb);
		win->img[size + 1] = mlx_get_color_value(win->mlx, (rgb >> 8));
		win->img[size + 2] = mlx_get_color_value(win->mlx, (rgb >> 16));
	}
	else
	{
		win->img[size] = mlx_get_color_value(win->mlx, (rgb >> 16));
		win->img[size + 1] = mlx_get_color_value(win->mlx, (rgb >> 8));
		win->img[size + 2] = mlx_get_color_value(win->mlx, rgb);
	}
}

void	put_texture(t_win *win, int x, int y, t_tex_cor *cor)
{
	int	size_win;
	int size_tex;

	size_win = (y * win->size_line) + (x * win->bpp / 8);
	size_tex = (cor->tex_y * win->tex[cor->tex].size_line)
		+ (cor->tex_x * win->tex[cor->tex].bpp / 8);
	win->img[size_win] = win->tex[cor->tex].img[size_tex];
	win->img[size_win + 1] = win->tex[cor->tex].img[size_tex + 1];
	win->img[size_win + 2] = win->tex[cor->tex].img[size_tex + 2];
}

void	draw_texture(t_win *win, int x, int y, t_wall *wall)
{
	t_tex_cor	cor;

	ft_bzero(&cor, sizeof(t_tex_cor));
	cor.tex = get_wall(win->game->player);
	if (x >= MAX_WIN || x < 0 || y >= MAX_WIN || y < 0)
		return ;
	if (win->game->player->hit == 1)
	{
		cor.tex_x = (win->game->player->h_hitx
				- floor(win->game->player->h_hitx)) * win->tex[cor.tex].width;
		cor.tex_y = ((y - wall->start) * win->tex[cor.tex].height)
			/ wall->height;
	}
	else if (win->game->player->hit == 2)
	{
		cor.tex_x = (win->game->player->v_hity
				- floor(win->game->player->v_hity)) * win->tex[cor.tex].width;
		cor.tex_y = ((y - wall->start) * win->tex[cor.tex].height)
			/ wall->height;
	}
	put_texture(win, x, y, &cor);
}

void	draw_raw(t_win *win, int x, int y)
{
	int wall;

	wall = get_wall(win->game->player);
	put_pixel(win, x, y, win->colours[wall]);
}

void	draw_column(t_win *win, int x, int start, int num_pixels)
{
	int		i;
	t_wall	wall;

	ft_bzero(&wall, sizeof(t_wall));
	wall.start = start;
	wall.height = num_pixels;
	i = start;
	while (i < start + num_pixels)
	{
		if (win->control->render_mode == true)
			draw_texture(win, x, i, &wall);
		else
			draw_raw(win, x, i);
		i++;
	}
}
