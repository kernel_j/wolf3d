/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycast.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/19 16:14:12 by jwong             #+#    #+#             */
/*   Updated: 2018/11/13 09:32:43 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "wolf3d.h"

int		is_hit(t_map *map, int x, int y)
{
	int ret;

	if (x < 0 || x >= map->col  || y < 0 || y >= map->row)
		ret = 2;
	else if (map->map[y][x] > 0)
		ret = 1;
	else
		ret = 0;
	return (ret);
}

double	get_intersect(t_player *player, double hsect, double vsect)
{
	double	dist;

	dist = 0;
	player->hit = 0;
	if (hsect >= 0 && vsect >= 0)
	{
		dist = (hsect < vsect) ? hsect : vsect;
		player->hit = (hsect < vsect) ? 1 : 2;
	}
	else if (hsect < 0 && vsect > 0)
	{
		dist = vsect;
		player->hit = 2;
	}
	else if (vsect < 0 && hsect > 0)
	{
		dist = hsect;
		player->hit = 1;
	}
	return (dist);
}

double	get_distance(t_map *map, t_player *player, double angle)
{
	double	hsect;
	double	vsect;
	double	wall_dist;
	double	dist;

	hsect = horizontal_intersect(map, player, angle);
	vsect = vertical_intersect(map, player, angle);
	wall_dist = get_intersect(player, hsect, vsect);
	dist = wall_dist * cos(M_PI / 180.0 * (angle - player->angle));
	return (dist);
}

void	floorcast(t_win *win, int col, double ray_angle, double start)
{
	double	beta_rad;
	double	dist;
    double  actual_dist;
    double  beta;
    double  alpha_rad;
	double	floor_x;
	double	floor_y;

    dist = 0.0;
    beta = ray_angle - win->game->player->angle;
	beta_rad = beta * M_PI / 180.0;
    alpha_rad = ray_angle * M_PI / 180.0;

    int row;
    row = start + 1;
    while (row < MAX_WIN)
    {
        dist = (PLANE_DIST * WALL_LEN) / (row - (MAX_WIN / 2.0)) / 2;
        actual_dist = dist / cos(beta_rad);
        floor_x = cos(alpha_rad) * actual_dist + win->game->player->pos_x;
        floor_x = (floor_x - floor(floor_x)) * win->tex[GND].width;
        floor_y = -sin(alpha_rad) * actual_dist + win->game->player->pos_y;
        floor_y = (floor_y - floor(floor_y)) * win->tex[GND].height;

        int size_win;
        int size_tex;

        size_win = (row * win->size_line) + (col * win->bpp / 8);
        size_tex = ((int)floor_y * win->tex[GND].size_line) + ((int)floor_x * win->tex[GND].bpp / 8);
        win->img[size_win] = win->tex[GND].img[size_tex];
        win->img[size_win + 1] = win->tex[GND].img[size_tex + 1];
        win->img[size_win + 2] = win->tex[GND].img[size_tex + 2];
        row++;
    }
}

void	skybox(t_win *win, int col, double ray_angle, double start)
{
	double	beta_rad;
	double	dist;
    double  actual_dist;
    double  beta;
    double  alpha_rad;
	double	floor_x;
	double	floor_y;

    dist = 0.0;
    beta = ray_angle - win->game->player->angle;
	beta_rad = beta * M_PI / 180.0;
    alpha_rad = ray_angle * M_PI / 180.0;

    int row;
    row = start - 1;
    while (row > 0)
    {
        dist = (PLANE_DIST * WALL_LEN) / (row - (MAX_WIN / 2.0)) / 10.0;
        actual_dist = dist / cos(beta_rad);
        floor_x = cos(alpha_rad) * actual_dist;
        floor_x = (floor_x - floor(floor_x)) * win->tex[SKY].width;
        floor_y = -sin(alpha_rad) * actual_dist;
        floor_y = (floor_y - floor(floor_y)) * win->tex[SKY].height;

        int size_win;
        int size_tex;

        size_win = (row * win->size_line) + (col * win->bpp / 8);
        size_tex = ((int)floor_y * win->tex[SKY].size_line) + ((int)floor_x * win->tex[SKY].bpp / 8);
        win->img[size_win] = win->tex[SKY].img[size_tex];
        win->img[size_win + 1] = win->tex[SKY].img[size_tex + 1];
        win->img[size_win + 2] = win->tex[SKY].img[size_tex + 2];
        row--;
    }

}

void	raycast(t_win *win)
{
	double	dist;
	double	height;
	double	angle;
	double	angle_inc;
	double	new_angle;
	int		i;

	angle_inc = FOV / MAX_WIN;
	angle = win->game->player->angle + (FOV / 2.0);
	i = 0;
	while(i < MAX_WIN)
	{
		new_angle = normalize_angle(angle - (angle_inc * i));
		dist = get_distance(win->game->map, 
				win->game->player, new_angle);
		height = calculate_height(dist);
		draw_column(win, i, find_start(height), height);
		if (win->control->render_mode)
		{
			skybox(win, i, new_angle, find_start(height));
			floorcast(win, i, new_angle, find_start(height) + height);
		}
		i++;
	}
}
