/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 12:33:39 by jwong             #+#    #+#             */
/*   Updated: 2018/11/08 17:18:03 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "libft.h"
#include "wolf3d.h"

void	init_win(t_win *win)
{
	win->mlx = mlx_init();
	win->win = mlx_new_window(win->mlx, MAX_WIN, MAX_WIN, "wolf3d");
	win->img_id = mlx_new_image(win->mlx, MAX_WIN, MAX_WIN);
	win->img = mlx_get_data_addr(win->img_id, &win->bpp, &win->size_line,\
			&win->endian);
}

void	init_player(t_player *player)
{
	player->pos_x += 0.5;
	player->pos_y += 0.5;
	player->angle = 45.0;
}

void	init_game(t_win *win, t_game *game, t_map *map, t_player *player)
{
	ft_bzero(game, sizeof(*game));
	ft_bzero(map, sizeof(*map));
	ft_bzero(player, sizeof(*player));
	player->pos_x = -1;
	player->pos_y = -1;
	win->game = game;
	win->game->map = map;
	win->game->player = player;
}

int		setup(char **argv, t_win *win, t_control *control)
{
	int	ret;

	ft_bzero(control, sizeof(*control));
	win->control = control;
	if ((ret = file_checker(argv[1], win)) == TRUE)
	{
		init_win(win);
		load_map(argv[1], win);
		init_player(win->game->player);
		return (ret);
	}
	return (ret);
}
