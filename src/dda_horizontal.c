/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda_horizontal.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 12:42:10 by jwong             #+#    #+#             */
/*   Updated: 2018/11/09 12:43:30 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "libft.h"
#include "wolf3d.h"

double	get_triangle_angle(double angle)
{
	double	new_angle;

	if (angle >= 0 && angle <= 90.0)
		new_angle = angle - 90.0;
	else if (angle > 90.0 && angle <= 180.0)
		new_angle = angle - 90.0;
	else if (angle > 180.0 && angle <= 270.0)
		new_angle = angle - 270.0;
	else
		new_angle = angle - 270.0;
	return (new_angle);
}

double	horizontal_intersect_down(t_map *map, t_player *player, t_dda *dda)
{
	double	dist;
	int		hit;

	dda->new_x = player->pos_x + dda->dx;
	player->h_ray_dir_y = dda->dy;
	player->h_ray_dir_x = dda->dx;
	while (!(hit = is_hit(map, floor(dda->new_x), (dda->new_y - 1))))
	{
		dda->dy -= 1;
		dda->new_y = player->pos_y + dda->dy;
		dda->dx = dda->dy * tan(dda->triangle_rad);
		dda->new_x = player->pos_x + dda->dx;
	}
	if (hit == 2)
		dist = -1;
	else
	{
		dist = calculate_distance(dda->dx, dda->dy);
		player->h_hitx = dda->new_x;
		player->h_hity = dda->new_y;
	}
	return (dist);
}

double	horizontal_intersect_up(t_map *map, t_player *player, t_dda *dda)
{
	double	dist;
	int		hit;

	dda->new_x = player->pos_x + dda->dx;
	player->h_ray_dir_y = dda->dy;
	player->h_ray_dir_x = dda->dx;
	while (!(hit = is_hit(map, floor(dda->new_x), dda->new_y)))
	{
		dda->dy += 1;
		dda->new_y = player->pos_y + dda->dy;
		dda->dx = dda->dy * tan(dda->triangle_rad);
		dda->new_x = player->pos_x + dda->dx;
	}
	if (hit == 2)
		dist = -1;
	else
	{
		dist = calculate_distance(dda->dx, dda->dy);
		player->h_hitx = dda->new_x;
		player->h_hity = dda->new_y;
	}
	return (dist);
}

void	horizontal_init_dda(t_dda *dda, t_player *player, double angle, bool up)
{
	double	triangle_angle;

	if (up == true)
		dda->new_y = ceil(player->pos_y);
	else
		dda->new_y = floor(player->pos_y);
	triangle_angle = get_triangle_angle(angle);
	dda->triangle_rad = triangle_angle * M_PI / 180;
	dda->dy = dda->new_y - player->pos_y;
	dda->dx = dda->dy * tan(dda->triangle_rad);
}

double	horizontal_intersect(t_map *map, t_player *player, double angle)
{
	t_dda	dda;
	double	dist;

	ft_bzero(&dda, sizeof(t_dda));
	if (angle > 0 && angle <= 180.0)
	{
		horizontal_init_dda(&dda, player, angle, false);
		dist = horizontal_intersect_down(map, player, &dda);
	}
	else if (angle > 180.0 && angle <= 360.0)
	{
		horizontal_init_dda(&dda, player, angle, true);
		dist = horizontal_intersect_up(map, player, &dda);
	}
	else
		dist = 0;
	return (dist);
}
