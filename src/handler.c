/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 12:44:25 by jwong             #+#    #+#             */
/*   Updated: 2018/11/08 17:16:11 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <math.h>
#include "mlx.h"
#include "wolf3d.h"

void	clean(t_win *win)
{
	mlx_destroy_image(win->mlx, win->img_id);
	mlx_destroy_window(win->mlx, win->win);
	exit(0);
}

int		key_press(int keycode, t_win *win)
{
	if (keycode == KEY_ESC)
		clean(win);
	if (keycode == KEY_W)
		win->control->up = true;
	if (keycode == KEY_S)
		win->control->down = true;
	if (keycode == KEY_A)
		win->control->left = true;
	if (keycode == KEY_D)
		win->control->right = true;
  if (keycode == KEY_TAB)
    win->control->render_mode = !win->control->render_mode;
  return (0);
}

int		key_release(int keycode, t_win *win)
{
	if (keycode == KEY_W)
		win->control->up = false;
	if (keycode == KEY_S)
		win->control->down = false;
	if (keycode == KEY_A)
		win->control->left = false;
	if (keycode == KEY_D)
		win->control->right = false;
	return (0);
}

int		mouse_over_handler(int x, int y, t_win *win)
{
	double	angle;

	if (x < 0 || x > MAX_WIN || y < 0 || y > MAX_WIN)
		return (0);
	angle = atan2(y, x);
	angle = angle * (M_PI / 180.0);
	if (x > MAX_WIN / 2)
		win->game->player->angle -= angle * 50;
	else
		win->game->player->angle += angle * 50;
	return (0);
}

int		exit_handler(t_win *win)
{
	clean(win);
	return (0);
}
