/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_checker.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/07 11:27:06 by jwong             #+#    #+#             */
/*   Updated: 2018/11/08 17:14:28 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <libft.h>
#include <wolf3d.h>

int	get_player_pos(t_win *win, int x, int y)
{
	t_player	*player;

	player = win->game->player;
	if (player->pos_x == -1 && player->pos_y == -1)
	{
		player->pos_x = calculate_len(x);
		player->pos_y = y;
	}
	else if (player->pos_x > -1 || player->pos_y > -1)
		return (FALSE);
	return (TRUE);
}

int	is_valid(char *line, t_win *win, int pos)
{
	int	i;

	i = 0;
	while (line[i])
	{
		if (!ft_isdigit(line[i]) && line[i] != ' ' && line[i] != 'P')
			return (FALSE);
		if (line[i] == 'P' && get_player_pos(win, i, pos) == FALSE)
			return (FALSE);
		i++;
	}
	if (win->game->map->col == 0)
		win->game->map->col = calculate_len(i);
	else if (win->game->map->col != calculate_len(i))
		return (FALSE);
	return (TRUE);
}

int	readfile(int fd, t_win *win)
{
	int		i;
	int		ret;
	char	*buff;

	i = 0;
	buff = NULL;
	while ((ret = get_next_line(fd, &buff)) > 0)
	{
		if (is_valid(buff, win, i) == FALSE)
		{
			free(buff);
			close(fd);
			printf("not valid\n");
			return (FALSE);
		}
		free(buff);
		buff = NULL;
		i++;
	}
	free(buff);
	win->game->map->row = i;
	if (win->game->player->pos_x == -1 && win->game->player->pos_y == -1)
		return (FALSE);
	return (TRUE);
}

int	is_file_empty(char *file)
{
	int		fd;
	int		ret;
	char	buff[3];

	fd = open(file, O_RDONLY);
	if (fd == -1)
		error_handler(FD_ERR);
	if ((ret = read(fd, buff, 3)) == -1)
		error_handler(FILE_EMPTY);
	if (ret <= 2)
	{
		if (ret == 0)
			error_handler(FILE_EMPTY);
		else if (ret == 1 || ret == 2)
			error_handler(FILE_ERR);
		close(fd);
		exit(-1);
	}
	return (FALSE);
}

int	file_checker(char *file, t_win *win)
{
	int	fd;
	int	ret;

	is_file_empty(file);
	fd = open(file, O_RDONLY);
	if (fd == -1)
		error_handler(FD_ERR);
	ret = readfile(fd, win);
	return (ret);
}
