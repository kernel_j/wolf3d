#include <math.h>
#include "wolf3d.h"

int		is_wall(t_map *map, double x, double y)
{
	int	new_x;
	int	new_y;
	int ret;

	new_x = floor(x);
	new_y = floor(y);
	if (new_x >= map->col || new_x < 0
			|| new_y >= map->row || new_y < 0)
		ret = TRUE;
	else if (map->map[new_y][new_x] == 0) 
		ret = FALSE;
	else
		ret = TRUE;
	return (ret);
}

void	calculate_move_distance_x(t_control *ctrl,
		double angle, double *x, double *padded)
{
    double  tmp;

	if (ctrl->up)
	{
        tmp = cos(angle);
		*x += tmp * MOVE_DIST;
        *padded += tmp * (MOVE_DIST + PADDING);
	}
	if (ctrl->down)
	{
        tmp = -cos(angle);
		*x += tmp * MOVE_DIST;
        *padded += tmp * (MOVE_DIST + PADDING);
	}
	if (ctrl->left)
	{
        tmp = -sin(angle);
		*x += tmp * MOVE_DIST;
        *padded += tmp * (MOVE_DIST + PADDING);
	}
	if (ctrl->right)
	{
        tmp = sin(angle);
		*x += tmp * MOVE_DIST;
        *padded += tmp * (MOVE_DIST + PADDING);
	}
}

void	calculate_move_distance_y(t_control *ctrl,
		double angle, double *y, double *padded)
{
    double tmp;

	if (ctrl->up)
	{
        tmp = -sin(angle);
		*y += tmp * MOVE_DIST;
        *padded += tmp * (MOVE_DIST + PADDING);
	}
	if (ctrl->down)
	{
        tmp = sin(angle);
		*y += tmp * MOVE_DIST;
        *padded += tmp * (MOVE_DIST + PADDING);
	}
	if (ctrl->left)
	{
        tmp = -cos(angle);
		*y += tmp * MOVE_DIST;
        *padded += tmp * (MOVE_DIST + PADDING);
	}
	if (ctrl->right)
	{
        tmp = cos(angle);
		*y += tmp * MOVE_DIST;
        *padded += tmp * (MOVE_DIST + PADDING);
	}
}
void	move_player(t_control *ctrl, t_player *player, t_map *map)
{
	double	angle;
	double	dist_x;
	double	dist_y;
    double  padded_x;
    double  padded_y;

	dist_x = 0;
	dist_y = 0;
    padded_x = dist_x;
    padded_y = dist_y;
	angle = player->angle * (M_PI / 180.0);
	calculate_move_distance_x(ctrl, angle, &dist_x, &padded_x);
	calculate_move_distance_y(ctrl, angle, &dist_y, &padded_y);
	if (is_wall(map, player->pos_x + padded_x, player->pos_y + padded_y) == FALSE)
	{
		player->pos_x += dist_x;
		player->pos_y += dist_y;
	}
}
