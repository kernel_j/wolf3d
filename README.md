# README #

This project was inspired by the 90s video game Wolfenstein 3D. It's purpose was to build an engine that renders 3D view using the technique ray-casting. 

![alt text](https://bitbucket.org/kernel_j/wolf3d/raw/867f98aa5fae014922fce501274dfd86799f0de0/img/basic_view.png)

![alt text](https://bitbucket.org/kernel_j/wolf3d/raw/867f98aa5fae014922fce501274dfd86799f0de0/img/tiled_view.png)

## Installation ##
The project comes with a makefile that compiles the program.

To compile the project, run the below command:
```
make
```

To re-compile:
```
make re
```

## Usage ##
The program takes a single parameter; the map.

To run:
```
./wolf3d map.txt
```

## Controls ##

Description | Key(s)
------------|-------
Close or quit | ESC
Move forward | UP Arrow or w
Move backward | DOWN arrow or s
Move left | LEFT arrow or a
Move right | RIGHT arrow or d
To change display mode | TAB
To rotate view | MOUSE movement

## Uninstall ##
To remove all object files:
```
make clean
```

To remove all object files and the executable:
```
make fclean
```

## Custom Maps ##
You can construct your own map by applying these rules:


* 0 represents empty space
* 1 represents walls
* P represents the player's initial position
