# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jwong <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/29 15:01:46 by jwong             #+#    #+#              #
#    Updated: 2018/09/27 13:50:44 by jwong            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= wolf3d

SRC		= dda_horizontal.c 	\
		  dda_vertical.c 	\
		  draw.c 			\
		  error.c			\
		  file_checker.c	\
		  init.c			\
		  handler.c			\
		  main.c			\
		  map.c				\
		  move_player.c 	\
		  normalize_angle.c	\
		  raycast.c			\
		  texture.c			\
		  util.c

CC		= gcc
DIR		= src/
SRCS	= $(addprefix $(DIR), $(SRC))
OBJ		= $(SRCS:.c=.o)

PLATFORM	:= $(shell uname)

ifeq	($(PLATFORM), Linux)
CFLAGS	= -Wall -Werror -Wextra -I libft/includes -I minilibx -I inc/
else ifeq	($(PLATFORM), Darwin)
CFLAGS	= -Wall -Werror -Wextra -I libft/includes -I minilibx_macos -I inc/
endif

all: $(NAME)

ifeq	($(PLATFORM), Linux)
$(NAME): $(OBJ)
		-make -C minilibx/
		make -C libft/
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L libft/ -L minilibx/ -lmlx -lXext -lX11 -lft -lm
		
%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		make -C minilibx/ clean
		make -C libft/ clean
		rm -f $(OBJ)

fclean: clean
		make -C libft/ fclean
		rm -f $(NAME)

re: fclean all

else ifeq	($(PLATFORM), Darwin)
$(NAME): $(OBJ)
		make -C minilibx_macos/
		make -C libft/
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L libft/ -L minilibx_macos/ -framework OpenGL -framework AppKit -lmlx -lft -lm
		
%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		make -C minilibx_macos/ clean
		make -C libft/ clean
		rm -f $(OBJ)

fclean: clean
		make -C libft/ fclean
		rm -f $(NAME)

re: fclean all

endif
