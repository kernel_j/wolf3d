#ifdef __APPLE__
# define KEY_TAB 48
# define KEY_ESC 53
# define KEY_LEFT 123
# define KEY_RIGHT 124
# define KEY_DOWN 125
# define KEY_UP 126
# define KEY_A 0
# define KEY_S 1
# define KEY_D 2
# define KEY_W 13
#elif __unix__
# define KEY_ESC 65307
# define KEY_LEFT 65361
# define KEY_RIGHT 65363
# define KEY_DOWN 65364
# define KEY_UP 65362
# define KEY_A 97
# define KEY_S 115
# define KEY_D 100
# define KEY_W 119
#endif

#ifndef WOLF3D_H
# define WOLF3D_H

#define MAX_WIN 1000
#define TRUE 0
#define FALSE -1
#define FOV 60.0
#define WALL_LEN 50
#define PLANE_DIST 20
#define MOVE_DIST 0.1
#define PADDING 0.9

#define ERRMSG_ARG "Error: Not enough arguments\n"
#define ERRMSG_FILE "Error: Invalid file\n"
#define ERRMSG_EMPTY_FILE "Error: empty file\n"
#define ERRMSG_TEXTURE "Error: cannot load textures\n"

#define TEX_BRICK  "textures/brick.xpm"
#define TEX_CELTIC "textures/celtic.xpm"
#define TEX_COLOURS "textures/colours.xpm"
#define TEX_STONE "textures/stone.xpm"
#define TEX_GND "textures/cobble.xpm"
#define TEX_SKY "textures/skybox.xpm"

#define WALL_NORTH 66047
#define WALL_EAST 8524820
#define WALL_SOUTH 1991780
#define WALL_WEST 13109840

#define NORTH 0
#define EAST 1
#define SOUTH 2
#define WEST 3
#define GND 4
#define SKY 5

#define NUM_TEX 6
#define NUM_WALL 4 

#include <stdbool.h>

enum            e_error
{
    ARG_ERR,
    FD_ERR,
    FILE_ERR,
    FILE_EMPTY,
    MALLOC_ERR,
	TEXTURE
};

typedef struct	s_player
{
	double		pos_x;
	double		pos_y;
	double		angle;
	double		h_hitx;
	double		h_hity;
	double		v_hitx;
	double		v_hity;
	double		v_ray_dir_x;
	double		v_ray_dir_y;
	double		h_ray_dir_x;
	double		h_ray_dir_y;
	int		hit;
}				t_player;

typedef struct	s_map
{
	int			row;
	int			col;
	int			**map;
}				t_map;

typedef struct	s_game
{
	t_player	*player;
	t_map		*map;
}				t_game;

typedef struct	s_control
{
	bool		up;
	bool		down;
	bool		left;
	bool		right;
    bool        render_mode;
}				t_control;

typedef struct	s_texture
{
	char		*img;
	void		*xpm;
	int			width;
	int			height;
	int			bpp;
	int			endian;
	int			size_line;
}				t_texture;

typedef struct	s_win
{
	void		*img_id;
	void		*mlx;
	void		*win;
	char		*img;
	int			bpp;
	int			endian;
	int			size_line;
    int         *colours;
	t_game		*game;
	t_control	*control;
	t_texture	*tex;
}				t_win;

typedef struct 	s_wall
{
	int 		height;
	int 		start;
}				t_wall;

typedef struct 	s_tex_cor
{
	int 		tex_x;
	int 		tex_y;
	int 		tex;
}				t_tex_cor;

typedef struct 	s_dda
{
	double	new_x;
	double	new_y;
	double	dx;
	double	dy;
	double	triangle_rad;
}				t_dda;

/*
** dda_horizontal.c
*/
double			horizontal_intersect(t_map *map, t_player *player, double angle);

/*
** dda_vertical.c
*/
double			vertical_intersect(t_map *map, t_player *player, double angle);

/*
** draw.c
*/
void			put_pixel(t_win *win, int x, int y, int rgb);
void			draw_column(t_win *win, int x, int start, int num_pixel);

/*
** error.c
*/
int             error_handler(int err);

/*
** file_checker.c
*/
int             file_checker(char *file, t_win *win);

/*
** handler.c
*/
int				key_press(int keycode, t_win *win);
int				key_release(int keycode, t_win *win);
int				mouse_over_handler(int x, int y, t_win *win);
int				exit_handler(t_win *win);

/*
** init.c
*/
int 			setup(char **argv, t_win *win, t_control *control);
void			init_game(t_win *win, t_game *game, t_map *map,
							t_player *player);
void			init_win(t_win *win);
void			init_player(t_player *player);

/*
** map.c
*/
void			load_map(char *file, t_win *win);

/*
** move_player.c
*/
void			move_player(t_control *ctrl, t_player *player, t_map *map);

/*
** normalize_angle.c
*/
double          normalize_angle(double angle);

/*
** raycast.c
*/
void			raycast(t_win *win);
int				is_hit(t_map *map, int x, int y);

/*
** texture.c
*/
void			load_texture(t_win *win);
void            load_colours(t_win *win);

/*
** util.c
*/
int 			calculate_len(int len);
int    			get_wall(t_player *player);
int				find_start(double height);
double			calculate_distance(double dx, double dy);
double			calculate_height(double dist);

#endif
